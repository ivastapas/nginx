FROM ubuntu:bionic

ENV NGINX_VERSION=1.19.4 \
    PCRE_VERSION=8.44 \
    ZLIB_VERSION=1.2.11 \
    OPENSSL_VERSION=1.1.1h

RUN apt update \
  && apt install -y --no-install-recommends build-essential wget\
  && rm -rf /var/lib/apt/lists/*

RUN wget --no-check-certificate https://nginx.org/download/nginx-${NGINX_VERSION}.tar.gz && tar zxvf nginx-${NGINX_VERSION}.tar.gz \
  && wget --no-check-certificate https://ftp.pcre.org/pub/pcre/pcre-${PCRE_VERSION}.tar.gz && tar zxvf pcre-${PCRE_VERSION}.tar.gz \
  && wget http://www.zlib.net/zlib-${ZLIB_VERSION}.tar.gz && tar xzvf zlib-${ZLIB_VERSION}.tar.gz \
  && wget --no-check-certificate https://www.openssl.org/source/openssl-${OPENSSL_VERSION}.tar.gz && tar xzvf openssl-${OPENSSL_VERSION}.tar.gz \
  && rm -rf *.tar.gz

RUN cd nginx-${NGINX_VERSION} && ./configure \
    --prefix=/usr/share/nginx \
    --sbin-path=/usr/sbin/nginx \
    --modules-path=/usr/lib/nginx/modules \
    --conf-path=/etc/nginx/nginx.conf \
    --pid-path=/run/nginx.pid \
    --lock-path=/var/lock/nginx.lock \
    --error-log-path=/var/log/nginx/error.log \
    --http-log-path=/var/log/nginx/access.log \
    --user=www-data \
    --group=www-data \
    --with-openssl=../openssl-${OPENSSL_VERSION} \
    --with-openssl-opt=enable-ec_nistp_64_gcc_128 \
    --with-openssl-opt=no-nextprotoneg \
    --with-openssl-opt=no-weak-ssl-ciphers \
    --with-openssl-opt=no-ssl3 \
    --with-pcre=../pcre-${PCRE_VERSION} \
    --with-pcre-jit \
    --with-zlib=../zlib-${ZLIB_VERSION} \
    --with-compat \
    --with-file-aio \
    --with-threads \
    --with-http_addition_module \
    --with-http_auth_request_module \
    --with-http_dav_module \
    --with-http_flv_module \
    --with-http_gunzip_module \
    --with-http_gzip_static_module \
    --with-http_mp4_module \
    --with-http_random_index_module \
    --with-http_realip_module \
    --with-http_slice_module \
    --with-http_ssl_module \
    --with-http_sub_module \
    --with-http_stub_status_module \
    --with-http_v2_module \
    --with-http_secure_link_module \
    --with-mail \
    --with-mail_ssl_module \
    --with-stream \
    --with-stream_realip_module \
    --with-stream_ssl_module \
    --with-stream_ssl_preread_module \
    --with-debug

RUN cd nginx-${NGINX_VERSION} && make && make install

RUN rm -rf nginx-${NGINX_VERSION} pcre-${PCRE_VERSION} zlib-${ZLIB_VERSION} openssl-${OPENSSL_VERSION}

COPY . /etc/nginx

EXPOSE 80/tcp 443/tcp

CMD ["/usr/sbin/nginx", "-g", "daemon off;"]
